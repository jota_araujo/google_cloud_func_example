const MongoClient = require("mongodb").MongoClient;
const axios = require("axios");
const auth = require("basic-auth");
const bcrypt = require("bcryptjs");
const moment = require("moment");
const yup = require("yup");
const setLocale = require("yup/lib/customLocale").setLocale;

const CUSTOM_OBJECT_ID = "10";

// maps source data to be saved as an existing Eloqua Custom Object
const mapFields = input => {
  const fields = [
    {
      type: "CustomObjectField",
      id: "315",
      value: input.name
      // "name": "First Name",
    },
    {
      type: "CustomObjectField",
      id: "316",
      value: ""
      // "name": "Last Name",
    },
      // [ ... ]
  ];
  return {
    type: "CustomObjectData",
    fieldValues: fields
  };
};

// Localize validation errors
setLocale({
  mixed: {
    default: "O atributo <${path}> é inválido",
    required: "O atributo <${path}> é obrigatório"
  },
  string: {
    min: "O atributo <${path}> deve conter pelo menos ${min} caracteres",
    max: "O atributo <${path}> deve conter no máximo ${max} caracteres",
    matches: "O atributo <${path}> é inválido"
  }
});

// constants
const RANK_INVALID_MESSAGE =
  "O atributo <rank> deve conter um dos valores: HOT, WARM ou COOL";
const LEAD_SCHEMA = yup.object().shape({
  name: yup
    .string()
    .min(3)
    .required(),
  rank: yup
    .string()
    .lowercase()
    .oneOf(["hot", "warm", "cool"], RANK_INVALID_MESSAGE),
  primaryPhoneAreaCode: yup
    .string()
    .min(2)
    .max(2)
    .matches(/[0-9]{2}/)
    .required(),
// [...]
  primaryPhoneNumber: yup
    .string()
    .min(8)
    .max(9)
    .matches(/[0-9]{8}/)
    .required(),
  emailLoja: yup
    .string()
    .matches(/@mycompany\.com\.br/)
    .required()
});

// ------ private functions ----------------------------
const formatError = str => ({ success: false, error: str });

const formatResponse = str => ({ success: true, message: str });

const validateInput = (schema, input) => {
  try {
    const res = schema.validateSync(input, { abortEarly: false });
    return [];
  } catch (err) {
    return err.errors;
  }
};


// maps the source data o an existing MongoDb document schema
const mapToDbEntry = input => ({
  name: input.name,
  rank: input.rank,
  primaryPhoneAreaCode: input.primaryPhoneAreaCode,
  primaryPhoneNumber: input.primaryPhoneNumber,
  description: input.description || "",
  primaryContactEmailAddress: input.primaryContactEmailAddress || "",
 // [ .. ]
  origem: input.origem || "",
  createdAt: moment().format(),
  updatedAt: moment().format()
});

const saveToDb = (conn, payload) => {
  return new Promise((resolve, reject) => {
    conn
      .db("myExampleDbName")
      .collection("leads")
      .insertOne(payload)
      .then(() => {
        resolve();
      })
      .catch(err => {
        reject(err);
      });
  });
};

const saveToEloqua = input => {
  const payload = mapFields(input);

  const clientId = process.env.ELOQUA_USER;
  const clientPass = process.env.ELOQUA_PASSWORD;
  console.log("Eloqua auth: " + clientId + ":" + clientPass);
  const authHeader = `Basic ${new Buffer.from(
    `${clientId}:${clientPass}`
  ).toString("base64")}`;

  const instance = axios.create({
    baseURL: "https://xxxxxxxx.eloqua.com/API/REST/2.0",
    timeout: 5000,
    headers: {
      Authorization: authHeader,
      "Content-Type": "application/json"
    }
  });
  const postPath = `/data/customObject/${CUSTOM_OBJECT_ID}/instance`;
  return instance.post(postPath, JSON.stringify(payload));
};

exports.createLead = (req, res) => {
  const input = req.body;
  if (!input) {
    res.status(400).send(formatError("Formato de requição JSON inválido"));
  }

  const errors = validateInput(LEAD_SCHEMA, input);
  if (errors.length) {
    res.status(400).send(formatError(errors[0]));
    return;
  }
  MongoClient.connect(process.env.DB_URI)
    .then(conn => {
      const dbEntry = mapToDbEntry(input);
      saveToDb(conn, dbEntry)
        .then(() => {
          saveToEloqua(input)
            .then(() => {
              console.log("DEBUG: Record added to Eloqua.");
              res
                .status(200)
                .send(formatResponse("Registro salvo com sucesso"));
            })
            .catch(err => {
              console.log(err);
              res
                .status(200)
                .send(formatResponse("Registro salvo com sucesso"));
            });
        })
        .catch(err => {
          console.log(err);
          res.status(500).send(formatError("Erro na gravação dos dados."));
        });
    })
    .catch(err => {
      console.log(err);
      res
        .status(500)
        .send(formatError("Falha na conexao com o banco de dados."));
    });
};